using System;

namespace ImportExcelFIle.DotNETCore.Models
{
    public class CommentModel
    {
        public string FbName { get; set; }
        public string FbId { get; set; }
        public string ASUId { get; set; }
        public string Comment { get; set; }
        public string PhoneNumber { get; set; }
    }
}
