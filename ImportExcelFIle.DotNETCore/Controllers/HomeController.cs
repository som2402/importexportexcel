﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ImportExcelFIle.DotNETCore.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Text.RegularExpressions;

namespace ImportExcelFIle.DotNETCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IHostingEnvironment _hostingEnvironment;
        private static IList<CommentModel> commentModels = new List<CommentModel>();
        public HomeController(ILogger<HomeController> logger, IHostingEnvironment hostingEnvironment)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> Import()
        {
            try
            {
                IFormFile file = Request.Form.Files[0];
                //string folderName = "UploadExcel";
                //string webRootPath = _hostingEnvironment.WebRootPath;
                //string newPath = Path.Combine(webRootPath, folderName);
                //if (!Directory.Exists(newPath))
                //{
                //    Directory.CreateDirectory(newPath);
                //}
                if (file.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                       await file.CopyToAsync(stream);

                        using (var package = new ExcelPackage(stream))
                        {
                            ExcelPackage.LicenseContext = LicenseContext.Commercial;

                            ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                            // get number of rows and columns in the sheet
                            int rows = worksheet.Dimension.Rows; // 20
                            int columns = worksheet.Dimension.Columns; // 7

                            Regex regexPhone = new Regex(@"(84|0[3|5|7|8|9])+([0-9]{8})\b");

                            for (int i = 2; i <= rows; i++)
                            {
                                string comment = string.Empty;
                                if (!string.IsNullOrWhiteSpace(worksheet.Cells[i, 4].Value?.ToString()))
                                {
                                   var comments = worksheet.Cells[i, 4].Value?.ToString().Split("\n")
                                        .GroupBy(x => x.ToLower())
                                        .Select(x => { 
                                            Match mathText = regexPhone.Match(x.Key);
                                            if (mathText.Success)
                                            {
                                                return x.Key;
                                            }
                                            return null;
                                        }).Where(m => !string.IsNullOrWhiteSpace(m)).ToList();

                                    comment = string.Join("\n", comments);
                                }

                                string phone = string.Empty;
                                if (!string.IsNullOrWhiteSpace(worksheet.Cells[i, 5].Value?.ToString()))
                                {
                                    var phones = worksheet.Cells[i, 5].Value?.ToString().Split("\n")
                                        .GroupBy(x => x.ToLower())
                                        .Select(x => {
                                             Match mathText = regexPhone.Match(x.Key);
                                             if (mathText.Success)
                                             {
                                                 return x.Key;
                                             }
                                             return null;
                                         }).Where(m => !string.IsNullOrWhiteSpace(m)).ToList();

                                    phone = string.Join("\n", phones);
                                }


                                var modelComment = new CommentModel()
                                {
                                    FbName = worksheet.Cells[i, 1].Value?.ToString(),
                                    FbId = worksheet.Cells[i, 2].Value?.ToString(),
                                    ASUId = worksheet.Cells[i, 3].Value?.ToString(),
                                    Comment = comment,
                                    PhoneNumber = phone,
                                };
                                commentModels.Add(modelComment);
                            }
                        }
                    }

                    //string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                    //string fullPath = Path.Combine(newPath, file.FileName);

                    //ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    //// If you use EPPlus in a noncommercial context
                    //// according to the Polyform Noncommercial license:
                    //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                    //FileInfo fileInfo = new FileInfo(fullPath);

                    //ExcelPackage package = new ExcelPackage(fileInfo);
                    //ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                    //// get number of rows and columns in the sheet
                    //int rows = worksheet.Dimension.Rows; // 20
                    //int columns = worksheet.Dimension.Columns; // 7

                    //// loop through the worksheet rows and columns
                    //for (int i = 2; i <= rows; i++)
                    //{
                    //    var modelComment = new CommentModel()
                    //    {
                    //        FbName = worksheet.Cells[i, 1].Value?.ToString(),
                    //        FbId = worksheet.Cells[i, 2].Value?.ToString(),
                    //        ASUId = worksheet.Cells[i, 3].Value?.ToString(),
                    //        Comment = worksheet.Cells[i, 4].Value?.ToString(),
                    //        PhoneNumber = worksheet.Cells[i, 5].Value?.ToString(),
                    //    };
                    //    commentModels.Add(modelComment);
                    //}
                }
                //return this.Content(sb.ToString());
                return Json(new { status = true, result = commentModels });
            }
            catch (Exception e)
            {
                Console.WriteLine("UPLOAD_EXCEL_ERROR: " + e.StackTrace);
                return Json(new { status = false, message = e.Message });
            }
        }

        public async Task<IActionResult> Export(bool isHasPhone, string keyword)
        {
            var irow = 2;
            int STT = 1;
            var path = Path.Combine(_hostingEnvironment.WebRootPath, "800k.xlsx");
            FileInfo fileInfo = new FileInfo(path);
            using (var package = new ExcelPackage(fileInfo, true))
            {
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                // If you use EPPlus in a noncommercial context
                // according to the Polyform Noncommercial license:
                //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                var workSheet = package.Workbook.Worksheets.FirstOrDefault();
                workSheet.View.ZoomScale = 90; // zoom file excel

                var dataComments = commentModels.Where(x => (isHasPhone == false || !string.IsNullOrWhiteSpace(x.PhoneNumber))
                        && !string.IsNullOrWhiteSpace(keyword) 
                        && (!string.IsNullOrWhiteSpace(x.Comment) && x.Comment.ToLower().Contains(keyword.ToLower())) 
                        || (!string.IsNullOrWhiteSpace(x.PhoneNumber) && x.PhoneNumber.ToLower().Contains(keyword.ToLower())) 
                        || (!string.IsNullOrWhiteSpace(x.FbName) && x.FbName.ToLower().Contains(keyword.ToLower())))
                    .ToList();
                foreach (var item in dataComments)
                {
                    workSheet.InsertRow(irow, 1);
                    workSheet.Cells[irow, 1].Value = item.FbName;
                    workSheet.Cells[irow, 2].Value = item.FbId;
                    workSheet.Cells[irow, 3].Value = item.ASUId;
                    workSheet.Cells[irow, 4].Value = item.Comment;
                    workSheet.Cells[irow, 5].Value = item.PhoneNumber;

                    workSheet.Cells[$"A{irow}:E{irow}"].Style.Font.Size = 11;
                    workSheet.Cells[$"A{irow}:E{irow}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[$"A{irow}:E{irow}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[$"A{irow}:E{irow}"].Style.WrapText = true;

                    //workSheet.Cells[$"A{irow}:E{irow}"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //workSheet.Cells[$"A{irow}:E{irow}"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //workSheet.Cells[$"A{irow}:E{irow}"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //workSheet.Cells[$"A{irow}:E{irow}"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    workSheet.Cells[$"A{irow}:E{irow}"].Style.Font.SetFromFont(new Font("Arial", 11));
                    irow += 1;
                }

                //workSheet.Cells[$"A{irow}:Y{irow}"].Style.Border.Top.Style = ExcelBorderStyle.Thin;

                package.Save();
                using (var buffer = package.Stream as MemoryStream)
                {
                    //return Ok(buffer.ToArray());
                    var byteResponse = buffer.ToArray();
                    return File(byteResponse, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"{DateTime.Now.ToString("ddMMyyyyHHmm")}_800k.xlsx");
                }
            }
        }
    }
}
